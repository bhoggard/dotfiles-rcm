export EDITOR=vim
. ~/.secrets

export PATH=~/.local/bin:$PATH

alias comp="docker-compose"
alias be="bundle exec"
# alias rake="noglob rake"
alias rub="git diff origin/master --name-only | xargs bundle exec rubocop -a"

alias gpc="hokusai production run --tty 'rails c production'"
alias gsc="hokusai staging run --tty 'rails c staging'"

alias pc="hokusai production run --tty 'rails c'"
alias sc="hokusai staging run --tty 'rails c'"

export HOKUSAI_ALWAYS_VERBOSE=True
