" vim:set ft=vim:
set number
set smartcase
set undodir=~/.undo
set cursorline
let g:use_cursor_shapes=1
filetype off
filetype plugin indent on
syntax on
set nocompatible
set backspace=indent,eol,start
set incsearch
set ruler
